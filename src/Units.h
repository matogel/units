#ifndef UNITS_H
#define UNITS_H

#include "Quantity.h"
// m kg s A K mol cd

template <typename T = double>
using Meter = Quantity<1,0,0,0,0,0,0, T>;

template <typename T = double>
using Kilogram = Quantity<0,1,0,0,0,0,0, T>;

template <typename T = double>
using Second = Quantity<0,0,1,0,0,0,0, T>;

template <typename T = double>
using Ampere = Quantity<0,0,0,1,0,0,0, T>;

template <typename T = double>
using Kelvin = Quantity<0,0,0,0,1,0,0, T>;

template <typename T = double>
using Mol = Quantity<0,0,0,0,0,1,0, T>;

template <typename T = double>
using Candela = Quantity<0,0,0,0,0,0,1, T>;



template <typename T = double>
using Newton = Quantity<1,1,-2,0,0,0,0, T>;

template <typename T = double>
using Pascale = Quantity<-1,1,-2,0,0,0,0, T>;

template <typename T = double>
using Volt = Quantity<2,1,-3,-1,0,0,0, T>;

template <typename T = double>
using Ohm = Quantity<2,1,-3,-2,0,0,0, T>;

template <typename T = double>
using Farad = Quantity<-2,-1,4,2,0,0,0, T>;

template <typename T = double>
using Coulomb = Quantity<0,0,1,1,0,0,0, T>;

template <typename T = double>
using Joule = Quantity<2,1,-2,0,0,0,0, T>;

template <typename T = double>
using Watt = Quantity<2,1,-3,0,0,0,0, T>;

template <typename T = double>
using Hertz = Quantity<0,0,-1,0,0,0,0, T>;

#endif // UNITS_H
