CXX = g++
LDFLAGS = -std=c++17
CXXFLAGS = -Wall -O3
LIBS = 


SOURCEDIR = src

BINDIR = bin


all: test
all: units

test: TARGETFILES = $(BINDIR)/test
test: SOURCES = $(SOURCEDIR)/test.cpp
test: executable

units: TARGETFILES = $(BINDIR)/units
units: SOURCES = $(SOURCEDIR)/main.cpp
units: executable

executable: $(SOURCES)
	mkdir -p $(BINDIR) && $(CXX) $(CXXFLAGS) $(LDFLAGS) $(SOURCES) -o $(TARGETFILES)

clean:
	rm -rf $(BINDIR)/*
