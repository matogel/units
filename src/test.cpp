#include <iostream>

#include "Units.h"
#include "Timer.h"

int main(void)
{
    int max = 100000000;

    Timer t;
    double total = 0;
    double av = 0;
    double avClass = 0;

    {
        for (int i = 0; i < max; i++)
        {
            t.restart();
            Meter<int> a(5);
            total += t.count<std::chrono::nanoseconds>();
        }
        avClass += total/double(max);
        total = 0;

    avClass /= double(max);
    std::cout<<"Defining Meter<int> took: "<<avClass<<" ns on average\n";
    av = 0;

        for (int i = 0; i < max; i++)
        {
            t.restart();
            int a{5};
            total += t.count<std::chrono::nanoseconds>();
        }
        av += total/double(max);
        total = 0;
    av /= double(max);
    std::cout<<"Defining int took: "<<av<<" ns on average\n";
    std::cout<<"The class took "<<avClass/av<<" of the time compared to basic types.\n";
    av = 0;
    avClass = 0;
    }

    {
    Meter<int> ma = 5;
    Meter<int> mb = 10;
    Quantity<2,0,0,0,0,0,0,int> mc = 0;

        for (int i = 0; i < max; i++)
        {
            t.restart();
            mc = ma*mb;
            total += t.count<std::chrono::nanoseconds>();
        }
        avClass += total/double(max);
        total = 0;
    avClass /= double(max);
    std::cout<<"Meter<int>*Meter<int> took: "<<avClass<<" ns on average\n";
    av = 0;

    int a{5};
    int b{10};
    int c{0};

        for (int i = 0; i < max; i++)
        {
            t.restart();
            c = a*b;
            total += t.count<std::chrono::nanoseconds>();
        }
        av += total/double(max);
        total = 0;
    av /= double(max);
    std::cout<<"int*int took: "<<av<<" ns on average\n";
    std::cout<<"The class took "<<avClass/av<<" of the time compared to basic types.\n";
    av = 0;
    avClass = 0;
    }

    {
    Meter<int> ma = 5;
    Meter<int> mb = 10;
    Quantity<0,0,0,0,0,0,0,int> mc = 0;

        for (int i = 0; i < max; i++)
        {
            t.restart();
            mc = ma/mb;
            total += t.count<std::chrono::nanoseconds>();
        }
        avClass += total/double(max);
        total = 0;
    avClass /= double(max);
    std::cout<<"Meter<int>/Meter<int> took: "<<avClass<<" ns on average\n";
    av = 0;

    int a{5};
    int b{10};
    int c{0};

        for (int i = 0; i < max; i++)
        {
            t.restart();
            c = a/b;
            total += t.count<std::chrono::nanoseconds>();
        }
        av += total/double(max);
        total = 0;
    av /= double(max);
    std::cout<<"int/int took: "<<av<<" ns on average\n";
    std::cout<<"The class took "<<avClass/av<<" of the time compared to basic types.\n";
    av = 0;
    avClass = 0;
    }
    {
    Meter<int> ma = 5;
    Meter<int> mb = 10;
    Meter<int> mc = 0;

        for (int i = 0; i < max; i++)
        {
            t.restart();
            mc = ma+mb;
            total += t.count<std::chrono::nanoseconds>();
        }
        avClass += total/double(max);
        total = 0;
    avClass /= double(max);
    std::cout<<"Meter<int>+Meter<int> took: "<<avClass<<" ns on average\n";
    av = 0;

    int a{5};
    int b{10};
    int c{0};

        for (int i = 0; i < max; i++)
        {
            t.restart();
            c = a+b;
            total += t.count<std::chrono::nanoseconds>();
        }
        av += total/double(max);
        total = 0;
    av /= double(max);
    std::cout<<"int+int took: "<<av<<" ns on average\n";
    std::cout<<"The class took "<<avClass/av<<" of the time compared to basic types.\n";
    av = 0;
    avClass = 0;
    }


    {
    Meter<int> ma = 5;
    Meter<int> mb = 10;
    Meter<int> mc = 0;

        for (int i = 0; i < max; i++)
        {
            t.restart();
            mc = ma-mb;
            total += t.count<std::chrono::nanoseconds>();
        }
        avClass += total/double(max);
        total = 0;
    avClass /= double(max);
    std::cout<<"Meter<int>-Meter<int> took: "<<avClass<<" ns on average\n";
    av = 0;

    int a{5};
    int b{10};
    int c{0};

        for (int i = 0; i < max; i++)
        {
            t.restart();
            c = a-b;
            total += t.count<std::chrono::nanoseconds>();
        }
        av += total/double(max);
        total = 0;
    av /= double(max);
    std::cout<<"int-int took: "<<av<<" ns on average\n";
    std::cout<<"The class took "<<avClass/av<<" of the time compared to basic types.\n";
    av = 0;
    avClass = 0;
    }
    return 0;
}
