#include <iostream>
#include <bitset>

#include "Units.h"
#include "Timer.h"

int main(void)
{

    Meter<double> d = 0.000008;
    Quantity<1,0,-1,0,0,0,0,double> w = 0.005;
    Quantity<-3,1,0,0,0,0,0,double> rho = 1000;
    Quantity<-1,1,-1,0,0,0,0,double> eta = 0.004;



    auto Re = rho*w*d/eta;
    std::cout<<Re<<" = "<<rho<<" * "<<w<<" * "<<d<<" / "<<eta<<"\n";

    auto test = eta.power<2>();

    std::cout<<test<<"\n";

    auto test2 = test.root<2>();

    std::cout<<test2<<"\n";
    return 0;
}
