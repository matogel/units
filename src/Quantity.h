#ifndef QUANTITY_H
#define QUANTITY_H

#include <cinttypes>
#include <ostream>
#include <cmath>

/**
 * Quantity
 * This class provides a compilation time check to prevent mistakes in unit usage
 *
 *
 */
template <int8_t m, int8_t kg, int8_t s, int8_t A, int8_t K, int8_t mol, int8_t cd, typename T = double>
class Quantity
{
public:
    T mValue;

    Quantity(const T &value) :
        mValue{value}
    {}

    Quantity(const Quantity<m,kg,s,A,K,mol,cd, T> &other) :
        mValue{other.mValue}
    {}

    T value() const
    {
        return mValue;
    }

    constexpr uint64_t repres() const
    {
        return ((255&uint64_t(m))<<48) | ((255&uint64_t(kg))<<40) | ((255&uint64_t(s))<<32) | ((255&uint64_t(A))<<24) | ((255&uint64_t(K))<<16) | ((255&uint64_t(mol))<<8) | cd;
    }

    static constexpr uint64_t representation()
    {
        return ((255&uint64_t(m))<<48) | ((255&uint64_t(kg))<<40) | ((255&uint64_t(s))<<32) | ((255&uint64_t(A))<<24) | ((255&uint64_t(K))<<16) | ((255&uint64_t(mol))<<8) | cd;
    }

    void operator=(const Quantity<m,kg,s,A,K,mol,cd, T> &other)
    {
        mValue = other.mValue;
    }

    void operator=(const T value)
    {
        mValue = value;
    }

    void operator+=(const Quantity<m,kg,s,A,K,mol,cd, T> &other) const
    {
        mValue += other.mValue;
    }

    void operator-=(const Quantity<m,kg,s,A,K,mol,cd, T> &other) const
    {
        mValue -= other.mValue;
    }

    Quantity<m, kg, s, A, K, mol, cd, T> operator+(const Quantity<m,kg,s,A,K,mol,cd, T> &other) const
    {
        return Quantity<m, kg, s, A, K, mol, cd, T>{mValue+other.mValue};
    }

    Quantity<m, kg, s, A, K, mol, cd, T> operator-(const Quantity<m,kg,s,A,K,mol,cd, T> &other) const
    {
        return Quantity<m, kg, s, A, K, mol, cd, T>{mValue-other.mValue};
    }

    template<int8_t om, int8_t okg, int8_t os, int8_t oA, int8_t oK, int8_t omol, int8_t ocd, typename oT = T, typename rT = T>
    Quantity<m+om, kg+okg, s+os, A+oA, K+oK, mol+omol, cd+ocd, rT> operator*(const Quantity<om, okg, os, oA, oK, omol, ocd, oT> &other) const
    {
        return Quantity<m+om, kg+okg, s+os, A+oA, K+oK, mol+omol, cd+ocd, rT>{mValue*other.mValue};
    }

    template<int8_t om, int8_t okg, int8_t os, int8_t oA, int8_t oK, int8_t omol, int8_t ocd, typename oT = T, typename rT = T>
    Quantity<m-om, kg-okg, s-os, A-oA, K-oK, mol-omol, cd-ocd, rT> operator/(const Quantity<om, okg, os, oA, oK, omol, ocd, oT> &other) const
    {
        return Quantity<m-om, kg-okg, s-os, A-oA, K-oK, mol-omol, cd-ocd, rT>{mValue/other.mValue};
    }

    template<int Power>
    Quantity<m*Power, kg*Power, s*Power, A*Power, K*Power, mol*Power, cd*Power, T> power() const
    {
        return Quantity<m*Power, kg*Power, s*Power, A*Power, K*Power, mol*Power, cd*Power, T>{pow(mValue, Power)};
    }

    template<int Root>
    Quantity<m/Root, kg/Root, s/Root, A/Root, K/Root, mol/Root, cd/Root, T> root() const
    {
        return Quantity<m/Root, kg/Root, s/Root, A/Root, K/Root, mol/Root, cd/Root, T>{pow(mValue, 1/double(Root))};
    }
    
    template <int8_t om, int8_t okg, int8_t os, int8_t oA, int8_t oK, int8_t omol, int8_t ocd>
    constexpr bool equalUnit(const Quantity<om,okg,os,oA,oK,omol,ocd, T> &other) const
    {
        return (m==om) && (kg==okg) && (s==os) && (A==oA) && (K==oK) && (mol==omol) && (cd==ocd);
    }

    bool operator==(const Quantity<m,kg,s,A,K,mol,cd, T> &other) const
    {
        return mValue==other.mValue;
    }

    bool operator!=(const Quantity<m,kg,s,A,K,mol,cd, T> &other) const
    {
        return mValue!=other.mValue;
    }

    bool operator<(const Quantity<m,kg,s,A,K,mol,cd, T> &other) const
    {
        return mValue<other.mValue;
    }

    bool operator>(const Quantity<m,kg,s,A,K,mol,cd, T> &other) const
    {
        return mValue>other.mValue;
    }

    bool operator<=(const Quantity<m,kg,s,A,K,mol,cd, T> &other) const
    {
        return mValue<=other.mValue;
    }

    bool operator>=(const Quantity<m,kg,s,A,K,mol,cd, T> &other) const
    {
        return mValue>=other.mValue;
    }

    friend std::ostream& operator<<(std::ostream& os, const Quantity<m,kg,s,A,K,mol,cd,T> unit)
    {
        os<<unit.mValue<<" ";

        bool hasQuantity = false;
        if (m != 0)
        {
            os<<"m";
            if (m != 1)
            {
                os<<"^"<<std::to_string(m);
            }
            hasQuantity = true;
        }
        if (kg != 0)
        {
            if (hasQuantity)
            {
                os<<"*";
            }
            hasQuantity = true;
            os<<"kg";
            if (kg != 1)
            {
                os<<"^"<<std::to_string(kg);
            }
        }
        if (s != 0)
        {
            if (hasQuantity)
            {
                os<<"*";
            }
            hasQuantity = true;
            os<<"s";
            if (s != 1)
            {
                os<<"^"<<std::to_string(s);
            }
        }
        if (A != 0)
        {
            if (hasQuantity)
            {
                os<<"*";
            }
            hasQuantity = true;
            os<<"A";
            if (A != 1)
            {
                os<<"^"<<std::to_string(A);
            }
        }
        if (K != 0)
        {
            if (hasQuantity)
            {
                os<<"*";
            }
            hasQuantity = true;
            os<<"K";
            if (K != 1)
            {
                os<<"^"<<std::to_string(K);
            }
        }
        if (mol != 0)
        {
            if (hasQuantity)
            {
                os<<"*";
            }
            hasQuantity = true;
            os<<"mol";
            if (mol != 1)
            {
                os<<"^"<<std::to_string(mol);
            }
        }
        if (cd != 0)
        {
            if (hasQuantity)
            {
                os<<"*";
            }
            hasQuantity = true;
            os<<"cd";
            if (cd != 1)
            {
                os<<"^"<<std::to_string(cd);
            }
        }

        return os;
    }
};

#endif // QUANTITY_H
